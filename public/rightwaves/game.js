"use strict";

const WIDTH = 100;
const HEIGHT = 96;
const BOTTOM_BAR_HEIGHT = 4;

const IMAGES = {
    "ship": {
        pixels: [
            "..www.......",
            "dddddww.bbb.",
            "rllldddcccwb",
            "dddhlllccccb",
            "rddddddaccb.",
            "..aaa......."
        ],
        key: {
            "w": [255, 255, 255],
            "d": [88, 88, 88],
            "b": [77, 111, 249],
            "r": [141, 0, 0],
            "l": [126, 125, 125],
            "h": [192, 192, 192],
            "c": [3, 157, 157],
            "a": [42, 42, 42]
        }
    },
    "life": {
        "pixels": [
            ".aa.....",
            "bbbccdae",
            "fcgbbdde",
            ".hh....."
        ],
        "key": {
            "a": [255, 255, 255],
            "b": [126, 125, 125],
            "c": [88, 88, 88],
            "d": [3, 157, 157],
            "e": [77, 111, 249],
            "f": [141, 0, 0],
            "g": [192, 192, 192],
            "h": [42, 42, 42]
        }
    },
    "machinery-20x8-01": {
        pixels: [
            "..abbbbbbbbbbbbbbb..",
            ".aaaaaaaaaaaaaaaaab.",
            "aacacacacccccccccccb",
            "daaaaaaaaaaaaaaaaaab",
            "dddadddaaaaaddddaaaa",
            ".dddeeddaaadeeedddd.",
            ".eefefeddddeeeeeee..",
            ".eeeeeeeeeeeaeaeae.."
        ],
        key: {
            "a": [184, 184, 184],
            "b": [255, 255, 255],
            "c": [67, 63, 16],
            "d": [119, 118, 118],
            "e": [45, 57, 22],
            "f": [117, 0, 0]
        }
    }
};

const LEVELS = [
    {
        scenery: [
            { x:  0, y: 88, image: "machinery-20x8-01"},
            { x: 20, y: 88, image: "machinery-20x8-01"}
        ]
    }
];

const game = new Smolpxl.Game();
//game.sendPopularityStats();
game.setFps(30);
game.showSmolpxlBar();
game.setSourceCodeUrl(
    "https://gitlab.com/andybalaam/smolpxl/-/blob/master/public/rightwaves/game.js"
);
game.setSize(WIDTH, HEIGHT + BOTTOM_BAR_HEIGHT);
game.setTitle("Rightwaves");

function new10Stars(list, speed) {
    for (let i = 0; i < 10; i++) {
        list.push([
            speed,
            Smolpxl.randomInt(0, WIDTH - 1),
            Smolpxl.randomInt(0, HEIGHT - 1)
        ]);
    }
}

function newStars() {
    let ret = [];
    new10Stars(ret, 0.2);
    new10Stars(ret, 0.5);
    new10Stars(ret, 1);
    return ret;
}

function newModel(oldModel, next_level) {
    return {
        screen: {
            x: 0
        },
        ship: {
            x: 20,
            y: (HEIGHT / 2) - (IMAGES["ship"].pixels.length / 2)
        },
        level: 0,
        keys: {
            UP: false,
            DOWN: false,
            LEFT: false,
            RIGHT: false,
            FIRE: false
        },
        stars: newStars()
    };
}

function updateInput(runningGame, model) {
    for (const input of runningGame.input()) {
        if (input.name === "UP") {
            model.keys.UP = true;
        } else if (input.name === "RELEASE_UP") {
            model.keys.UP = false;
        } else if (input.name === "DOWN") {
            model.keys.DOWN = true;
        } else if (input.name === "RELEASE_DOWN") {
            model.keys.DOWN = false;
        } else if (input.name === "LEFT") {
            model.keys.LEFT = true;
        } else if (input.name === "RELEASE_LEFT") {
            model.keys.LEFT = false;
        } else if (input.name === "RIGHT") {
            model.keys.RIGHT = true;
        } else if (input.name === "RELEASE_RIGHT") {
            model.keys.RIGHT = false;
        }
    }
}

function updateMoveShip(model) {
    if (model.keys.UP) {
        model.ship.y -= 1;
    }
    if (model.keys.DOWN) {
        model.ship.y += 1;
    }
    if (model.keys.LEFT) {
        model.ship.x -= 1;
    }
    if (model.keys.RIGHT) {
        model.ship.x += 1;
    }
}

function updateScroll(model) {
    model.screen.x += 0.5;
    model.ship.x += 0.5;

    model.stars = model.stars.map(
        star => (
            star[1] - (model.screen.x * star[0]) < 0
                ? [star[0], star[1] + WIDTH, star[2]]
                : star
        )
    );
}

function update(runningGame, model) {
    updateInput(runningGame, model);
    updateMoveShip(model);
    updateScroll(model);
    return model;
}

function starColor(speed) {
    return [100 * speed, 100 * speed, 200 * speed];
}

function viewStars(screen, model) {
    for (const [speed, x, y] of model.stars) {
        screen.set(x - (model.screen.x * speed), y, starColor(speed));
    }
}

function viewShip(screen, model) {
    const ship_image = IMAGES["ship"];
    screen.draw(
        model.ship.x - model.screen.x,
        model.ship.y,
        ship_image.pixels,
        ship_image.key
    );
}

function viewScenery(screen, model) {
    const level = LEVELS[model.level];
    for (const scenery of level.scenery) {
        const image = IMAGES[scenery.image]
        screen.draw(
            scenery.x - model.screen.x,
            scenery.y,
            image.pixels,
            image.key
        );
    }
}

function viewBottomBar(screen, model) {
    const life_image = IMAGES["life"];
    screen.rect(0, HEIGHT, WIDTH, BOTTOM_BAR_HEIGHT, Smolpxl.colors.BLACK);
    for (let i = 0; i < 3; ++i) {
        screen.draw(1 + i * 10, HEIGHT, life_image.pixels, life_image.key);
    }
}

function view(screen, model) {
    viewStars(screen, model);
    viewShip(screen, model);
    viewScenery(screen, model);
    viewBottomBar(screen, model);
}

game.start("rightwaves", newModel(), view, update);
